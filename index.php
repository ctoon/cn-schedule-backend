<?php
// Composer
require __DIR__ . '/vendor/autoload.php';

// .env
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

// Whoops
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
if ($_ENV['APP_ENV'] == 'prod') {
  // Hide sensitive stuff in production
  $whoops->pushHandler(function($exception, $inspector, $run) {
    $k = [
      'DB_HOST',
      'DB_PORT',
      'DB_USER',
      'DB_PASS',
      'DB_NAME'
    ];
    foreach ($k as $key) {
      $_ENV[$key] = '🍑';
      $_SERVER[$key] = '🍑';
    }
  });
}
$whoops->register();

// Fat Free Framework
$f3 = \Base::instance();

// The config
$f3->config(__DIR__ . '/app/config.ini');

// The DB
$db = new DB\SQL(
  'mysql:host=' . $_ENV['DB_HOST'] .
  ';port=' . $_ENV['DB_PORT'] .
  ';dbname=' . $_ENV['DB_NAME'],
  $_ENV['DB_USER'],
  $_ENV['DB_PASS']
);
$f3->set('db', $db);

// CORS and content-type
$f3->set('CORS.origin', '*');
header('Content-Type: application/json');

// Here we go
$f3->run();
