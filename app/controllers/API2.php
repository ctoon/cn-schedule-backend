<?php
class API2 {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('days', new DB\SQL\Mapper($db, 'days'));
    $f3->set('stats', new DB\SQL\Mapper($db, 'stats'));
  }

  public function error($f3) {
    $output = array('error' => array(
      'code' => $f3->get('ERROR.code'),
      'message' => $f3->get('ERROR.text')
    ));

    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  private function decodeJson($json) {
    $d = [];

    $d['cn'] = $json['cn'] ? json_decode($json['cn']) : null;
    $d['zap'] = $json['zap'] ? json_decode($json['zap']) : null;
    $d['tvguide'] = $json['tvguide'] ? json_decode($json['tvguide']) : null;
    $d['as'] = $json['as'] ? json_decode($json['as']) : null;

    return $d;
  }

  private function makeDay($schedule, $stats) {
    $day = array(
      'schedule' => [],
      'stats' => []
    );

    // Get selected date and the last update
    $day['date'] = $schedule['date'];
    $day['lastupdate'] = $schedule['lastupdate'];

    // Decode schedules' json
    $day['schedule'] = $this->decodeJson($schedule);

    // Decode stats' json
    $day['stats'] = $this->decodeJson($stats);

    // Check if everything is null (because some schedule is incomplete)
    $allnull = true;
    foreach ($day['schedule'] as $t) {
      if (!is_null($t)) {
        $allnull = false;
        break;
      }
    }

    if ($allnull) return null;

    return $day;
  }

  public function days($f3) {
    $output = [];

    // Load all entries
    $f3->get('days')->load(
      null,
      array('order' => 'date DESC')
    );

    // And save the date value
    while(!$f3->get('days')->dry()) {
      $output[] = $f3->get('days')->date;
      $f3->get('days')->next();
    }

    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function day($f3) {
    $day = [];

    // Load schedule and stats for the day
    $f3->get('days')->load(
      array('date=?', $f3->get('PARAMS.date'))
    );
    $f3->get('stats')->load(
      array('date=?', $f3->get('PARAMS.date'))
    );

    // Make our json if there's something
    if (!$f3->get('days')->dry()) {
      $output = $this->makeDay(
        $f3->get('days')->cast(),
        $f3->get('stats')->cast()
      );
    }

    // Check for 404
    if (empty($output)) {
      $f3->error(404, 'There isn\'t any data available about that day.');
      return;
    }

    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function range($f3) {
    $output = [];
    $current = false;

    // For /current
    if (is_null($f3->get('PARAMS.start'))) {
      $tz = new DateTimeZone('America/New_York');
      $d = new DateTime();
      $d->setTimezone($tz);
      $f3->set('PARAMS.start', $d->format('Y-m-d'));
      $f3->set('PARAMS.end', '2099-12-31');
      $current = true;
    }

    // Fix order
    if ($f3->get('PARAMS.start') > $f3->get('PARAMS.end')) {
        $t = $f3->get('PARAMS.start');
        $f3->set('PARAMS.start', $f3->get('PARAMS.end'));
        $f3->set('PARAMS.end', $t);
    }

    // Don't allow more than 30 days
    $d1 = new DateTime($f3->get('PARAMS.start'));
    $d2 = new DateTime($f3->get('PARAMS.end'));
    $diff = $d1->diff($d2);
    if (!$current && $diff->format('%a') > 30) {
        $d2->sub(new DateInterval('P30D'));
        $f3->set('PARAMS.start', $d2->format('Y-m-d'));
    }

    // Load schedule and stats for the range
    $f3->get('days')->load(
      array('date >= ? AND date <= ?', $f3->get('PARAMS.start'), $f3->get('PARAMS.end')),
      array('order' => 'date ASC')
    );
    $f3->get('stats')->load(
      array('date >= ? AND date <= ?', $f3->get('PARAMS.start'), $f3->get('PARAMS.end')),
      array('order' => 'date ASC')
    );

    // Go through all selected days
    while(!$f3->get('days')->dry()) {
      // Make our json
      $day = $this->makeDay(
        $f3->get('days')->cast(),
        $f3->get('stats')->cast()
      );

      // Check if it's not null
      if (!is_null($day)) $output[$f3->get('days')['date']] = $day;

      $f3->get('days')->next();
      $f3->get('stats')->next();
    }

    // Check for a 404
    if (empty($output)) {
      $f3->error(404, 'There isn\'t any data available in that range.');
      return;
    }

    echo json_encode($output, JSON_PRETTY_PRINT);
  }
}
